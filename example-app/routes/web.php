<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('', [HomeController::class, 'index']);
Route::post('print-order',[HomeController::class, 'printOrder']);
Route::post('photo-order', [HomeController::class, 'photoOrder']);
Route::get('image', function (){
    return view('image');
});
