$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".btn-print").click(function () {
        $.ajax({
            url: window.location.origin + '/print-order',
            type: 'post',
            data: null,
            dataType: 'json',
            success: function (data) {
                if (data.status){
                    newWin= window.open("");
                    newWin.document.write(data.view);
                }else{
                    console.log(data.msg);
                }
            }
        });
    });

    $(".btn-photo").click(function () {
        $.ajax({
            url: window.location.origin + '/photo-order',
            type: 'post',
            data: null,
            dataType: 'json',
            success: function (data) {
                if (data.status){
                    newWin= window.open("");
                    newWin.document.write(data.view);
                }else{
                    console.log(data.msg);
                }
            }
        })
    });
});
