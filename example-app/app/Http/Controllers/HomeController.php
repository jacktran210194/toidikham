<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Knp\Snappy\Pdf;

class HomeController extends Controller
{
    public function index ()
    {
        return view('welcome');
    }

    public function printOrder (Request $request)
    {
        $user = User::first();
        $view = view('print', compact('user'))->render();
        return response()->json(['status' => true, 'view' => $view]);
    }

    public function photoOrder (Request $request)
    {
        $user = User::first();
        $view = view('image', compact('user'))->render();
        return response()->json(['status' => true, 'view' => $view]);
    }
}
