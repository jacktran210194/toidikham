
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>In đơn hàng</title>
    <style type="text/css">
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        body,
        html {
            margin: 0;
            padding: 0;
        }
        .content {
            width: 196px;
            height: 245px;
            margin: 0;
            font-weight: bold;
        }

        .content:last-child {
            height: 245px;
        }

        .content.page-break {
            display: block;
            page-break-after: always;
        }

        .content.page-break:last-child {
            display: block;
            page-break-after: avoid;
        }

        .row {
            display: inline-block;
            width: 100%;
        }

        .col-2,
        .col-3,
        .col-4,
        .col-5,
        .col-7,
        .col-8,
        .col-9,
        .col-10 {
            float: left;
        }

        .col-2 {
            width: 16.66%;
        }

        .col-3 {
            width: 22%;
        }

        .col-4 {
            width: 33.33%;
        }

        .col-5 {
            width: 41.66%;
        }

        .col-6 {
            width: 50%;
        }

        .col-7 {
            width: 58.34%;
        }

        .col-8 {
            width: 66.66%
        }

        .col-9 {
            width: 78%;
        }

        .col-10 {
            width: 83.33%;
        }

        .info-user {
            margin-top: 2px;
            margin-left: 3px;

            max-height: 56px;
            width: 100%;
            text-transform: uppercase;
            font-size: 8px;
        }

        .info-user p {
            margin: 0 5px 1px 0;
        }

        .info-note {
            display: table;
            height: 32px;
            width: 100%;
        }

        .info-note .col-9 {
            width: 75%;
            margin-left: 3px;
            border-top: 1px dotted black;
            border-bottom: 1px dotted black;
            border-right: 1px dotted black;
            height: 32px;
        }

        .info-note .col-3 {
            padding-left: 3px;
            border-top: 1px dotted black;
            border-bottom: 1px dotted black;
        }

        .info-note p {
            font-size: 7px;
        }

        p {
            margin: 0;
        }

        body {
            font-family: "Arial";
            size: 196px 245px;
            margin: 0;
            min-height: auto;
        }

        #delivery {
            display: table;
            height: 47px;
            width: 100%;
            text-transform: uppercase;
            font-size: 12px;
            font-weight: bold;
        }

        #wardcode {
            margin: 4px 0px 6px 3px;
            height: 14px;
            display: block;
        }

        #DistrictEncode {
            margin-left: 3px;
            padding-left: 1px;
            display: inline;
            border: 1px solid black;
            display: block;
        }

        #date {
            margin: 0 0 0 20px;
            margin-left: 35px;
            font-size: 17px;
            color: black;
        }

        #signature {
            height: 32px;
            font-size: 8px;
            color: black;
            padding-left: 4px;
            padding-top: 8px;
            font-weight: normal;
        }

        #external {
            padding-top: 1px;
            margin-left: 3px;
            display: table;
            width: 96%;
            height: 14px;
            border-bottom: 1px solid black;
            text-align: center;
            font-size: 10px;
            font-weight: bold;
            text-transform: uppercase;
        }

        #sorting {
            padding-top: 1px;
            margin-left: 3px;
            display: table;
            width: 96%;
            height: 18px;
            border-bottom: 1px solid black;
            text-align: center;
            font-size: 14px;
            font-weight: bold;
            text-transform: uppercase;
        }

        #barcode {
            display: table;
            height: 48px;
            width: 100%;
            text-align: center;
            font-size: 10px;
            font-weight: bold;
            text-transform: uppercase;
        }

        #barcode img {
            width: 180px;
            height: 30px;
        }

        .w3-light-grey,
        .w3-hover-light-grey:hover,
        .w3-light-gray,
        .w3-hover-light-gray:hover {
            color: #000!important;
            background-color: #f1f1f1!important
        }

        .w3-round,
        .w3-round-medium {
            border-radius: 4px
        }

        .w3-container,
        .w3-panel {
            padding: 0.01em 16px
        }

        .w3-green,
        .w3-hover-green:hover {
            color: #fff!important;
            background-color: #ff8328!important
        }

        .w3-round-large {
            border-radius: 8px
        }

        #process-bar {
            margin:auto;
            position: fixed;
            z-index: 1000;
            width: 98%;
            height: 20px;
            top:  50%;
            left: 50%;
            transform: translate(-50%,-50%);
        }
        .line-clamp-3 {
            display: -webkit-box;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
        }
        .line-clamp-2 {
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
        }
        .line-clamp-1 {
            display: -webkit-box;
            -webkit-line-clamp: 1;
            -webkit-box-orient: vertical;
        }
        .module {
            overflow: hidden;
        }
    </style>
</head>
<body>
<div class="content page-break" id="html-content-holder" style="margin-bottom: 0px; padding:2px">
    <div id="info">
        <div id="delivery-GAHFPLFB">
            <div style="padding: 2px; font-size: 10px; border-bottom: 1px dotted">
                <div style="display: flex; align-items: center; justify-content: space-between; margin-top: 2px">
                    <div style="font-size: 11px; line-height: 15px">Họ Tên Bệnh Nhân</div>
                    <div style="font-size: 11px; line-height: 15px">Tuổi</div>
                </div>
                <div style="display: flex; align-items: center; justify-content: space-between">
                    <div>{{$user->email}}</div>
                    <div style="font-size: 19px; line-height: 17px">28</div>
                </div>
            </div>
        </div>
        <div class="info-user">
            <div class="row">
                <p id="name-receiver-GAHFPLFB" style="font-weight: bold;">
                    Họ Tên Bác Sĩ
                </p>
                <p id="phone-receiver-GAHFPLFB">
                    MR. Trần Đông
                </p>
                <p id="address-receiver-GAHFPLFB" style="max-height: 60px;">
                    Chuyên Khao : Răng hàm mặt
                </p>
            </div>
        </div>
        <div>
            <div class="row">
                <div class="col-9">
                    <p id="note-field-GAHFPLFB" style="text-transform: uppercase; padding: 2px 0; height: 18px;">
                        STT: 15
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div id="barcode">

        <div
            style="display: flex; justify-content:  center ; align-items: flex-end; padding-top: 2px;"
        >

            <p style="padding: 1px">Mã QR</p>

        </div>
        {{--        Điền thông tin hoặc link thay chữ hello (Viết không dấu)--}}
        {!! QrCode::generate('Add link or info'); !!}

    </div>


</div>
<script src="html2canvas.min.js" type="text/javascript"></script>
<script>
    setTimeout(function () {
        html2canvas(document.getElementById("html-content-holder")).then(function (canvas) {
            let anchorTag = document.createElement("a");
            document.body.appendChild(anchorTag);
            anchorTag.download = "filename.jpg";
            anchorTag.href = canvas.toDataURL();
            anchorTag.click();
        });
    }, 200);
</script>
</body>

